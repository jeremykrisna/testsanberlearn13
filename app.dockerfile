FROM php:7.2-fpm-alpine

WORKDIR /var/www/html

RUN docker-php-ext-install mysqli
RUN docker-php-ext-install -j$(nproc) pdo_mysql 

EXPOSE 9000

CMD php-fpm